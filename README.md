# norjohe

Not really a linker. Harshly reduces the file size of static, standalone,
PLT- and GOT-less ELF binaries.

## Usage

```
Links a static ELF executable into a smaller one.

NOTE: the following things must *NOT* appear in the input binary:
	* ~~Position-dependent code~~ Should work? Kinda, at least.
	* Dynamically linked symbols
	* ~~.data, .rodata, .bss and~~ .hash sections
	  * .data and .bss are supported as of v0x02. .rodata isn't supported
	    out of the box, just merge it with .text in your linker script.
	* Initializers and finalizers (unless handled manually)
	* A GOT or PLT
	* Exception handler frame data

These conditions are usually fulfilled when one compiles & links with
`-nostdlib -nostartfiles -static -fno-plt (-fno-pie -no-pie)'.

Currently, only x86 and x86_64 binaries are supported.

Usage:
norjohe	[-h|--help] [-V|--version] [-i|--pic] [-p|--python]
		<input file> <output file>

	-h	Display this help text and exit.
	-V	Print the version number and exit.
	-i	Use another hack to make the resulting file size even smaller,
		but this only works when the program contains no absolute memory
		accesses.
	-p	Use a Python loader instead of an ELF header. Has the same
		limitations as the `-i' option. Usage of this flag requires
		Python to be installed.

		The `-i' and `-p' flags cannot be combined. If one of these flags
		is used an the binary does contain absolute memory accesses, norjohe
		will succeed but the output will most likely segfault.

norjohe needs the program `elftoc', a C compiler
and the YASM assembler in order to operate. The programs used for
this can be controlled using the NRJH_{ELFTOC,CC,ASM} environment variables.
The directory of the `resource' files (i.e. the C and assembly templates)
can be selected using the NRJH_RES variable, which defaults to
`$PREFIX/libexec/norjohe'.
```

## Dependencies

* `A C compiler`
* `yasm` (*not* `nasm`, the latter breaks)
* `elftoc` (from the `ELFKickers` 'package')
* `python` (optional, if you want to use the `-p` flag)

## Installing

```
./install.sh
```

Optionally, the `PREFIX` envvar can be used to specify the installation
directory. It defaults to `$HOME/.local`

## Inner working

The input file is 'disassembled' using `elftoc`, which is then combined with
a C program that analyzes the contents and extracts the `.text` section.

This section is then dumped to a binary file, which is `incbin`'ed by a NASM
assembly source with a handwritten ELF header. This file is then assembled to
produce the final binary.

Yep, it's completely crazy.

## Comparison with other linkers

*NOTE: the following paragraph is valid for norjohe v. 0x01, current version is
0x02 and this test hasn't been redone yet.*

Tested using my 1/2/4k 'introsystem' (`x86_64`).

| linker   | norjohe | BOLD | GNU ld |
|----------|--------:|-----:|-------:|
| size (b) | 615     | 916  | 1784   |

Flags used with BOLD:

    --raw

Flags used with ld:

    -nostdlib -nostartfiles -static

## If you want to dynamically import functions

Don't use norjohe. Use BOLD or something.

## Greets

* arlj for `bold`
* breadbox for `elftoc` and his teensy ELF headers

Fuckings to those who can't survive without using dynamic libs

## LICENSE

[SAL](/LICENSE)

