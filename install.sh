#!/usr/bin/env bash

set -e

escape_for_sed() {
    echo "$1" | sed -E 's/[\/&]/\\&/g'
}

if [[ -z "$PREFIX" ]]; then
    PREFIX="$HOME/.local"
fi

if [[ "$1" == "--help" ]] || [[ "$1" == "-h" ]]; then
    echo "[PREFIX=...] $(basename "$0")"
    exit
fi

echo "\$PREFIX is '${PREFIX}'"

cd "$(dirname "$0")"

mkdir -vp "$PREFIX/bin" "$PREFIX/libexec/norjohe"

< src/norjohe.sh sed -E "s/INSTALL_PREFIX/$(escape_for_sed "$PREFIX")/" \
    > "$PREFIX/bin/norjohe"
echo "Installed $PREFIX/bin/norjohe"
chmod -v +x "$PREFIX/bin/norjohe"

cp -vf src/linker.pre.c \
    src/elfhdr-32.pre.asm src/elfhdr-64.pre.asm \
    src/loader-32.pre.py  src/loader-64.pre.py  \
    "$PREFIX/libexec/norjohe/"

