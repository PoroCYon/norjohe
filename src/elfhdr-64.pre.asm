
; to define:
; * START_OFF
; * ADDR_TEXT
; * EI_OSABI    ; not really needed
; * TEXT_OFF
; * IS_PIC

%ifdef HAS_DATA
%error "E: .data/.bss not supported on x86_64 yet. Please send patches to"
%error "   PoroCYon/K2."
%endif

%ifndef PAGE_SIZE
%define PAGE_SIZE 0x1000
%endif

%ifndef EXTRA_ZEROS
%define EXTRA_ZEROS 0
%endif

%ifdef IS_PIC
%define ADDR_TEXT 0x100000000
;%define START_OFF 0
%define TEXT_OFF ahdrsize
%endif

bits 64
org ADDR_TEXT

ehdr:
    db 0x7F, "ELF"  ;!e_ident       ;!EI_MAG[0-3]
    db  0           ; EI_CLASS      ; 1: 32-bit, 2: 64-bit
    db 'K'          ; EI_DATA       ; 1 = 2's compl., LE ; 2 = 2's compl., BE
    db '2'          ; EI_VERSION    ; 1 = current
    db  0           ; EI_OSABI      ; 0 = SYSV, 3 = GNU/Linux, 2 = NetBSD, 9 = FreeBSD, 12 = OpenBSD
    db "pcy!nrjh"   ; padding
    dw  2           ;!e_type        ; 2 = executable, 3 = shared obj, 4 = coredump
    dw 62           ;!e_machine     ; 3 = x86, 62 = x86_64
    dd  0           ; e_version     ; 1 = current
%ifndef IS_PIC
    dq blob + START_OFF ;!e_entry
    dq phdr - ehdr  ;!e_phoff
    dq 0            ; e_shoff
    dd 0            ; e_flags
    dw 0            ; e_ehsize
    dw phdrsize     ;!e_phentsize
phdr: ; TODO: recheck for more merging
    dd 1            ;!p_type        ;!e_phnum, e_shentsize  : needs at least one of 1|4, one implies the other
    dd 1            ;~p_flags       ; e_shnum, e_shstrndx   ; ^ + kernel only looks at LSbyte of p_flags ->
    dq 0            ;!p_offset                              ; rest can be nonsense (syscall, jmp short $foo)
    dq ADDR_TEXT    ;!p_vaddr
    dq 0            ; p_paddr
    dq filesize     ;~p_filesz      ; -> can be anyting >=filesize && < 1<<32, but have to be equal
    dq filesize     ;~p_memsz       ; -> ^
    dq 0            ; p_align
%else
    dd blob + START_OFF - ehdr  ;!e_entry (lo)
phdr:
    dd 1                        ;         (hi)  ;!p_type
    dd phdr - ehdr              ;!e_phoff (lo)  ;~p_flags   ; see note above
    dd 0                                        ;!p_offset
    dd 0                        ; e_shoff
    dq ADDR_TEXT                                ;!p_vaddr
                                ; e_flags
    dw 0 ; 0x40 if it breaks    ; e_ehsize      ; p_paddr
    dw phdr_end-phdr            ;!e_phentsize
    dw 1                        ;!e_phnum
    dw 0                        ; e_shentsize
    dq filesize                 ; e_shnum       ;~p_filesz  ; see note above
                                ; e_shstrndx
    dq filesize                                 ;~p_memsz   ; see note above
    dq 0                                        ; p_align
%endif

phdr_end:
    times (TEXT_OFF - ahdrsize) db 0 ; ensure the blob starts at the old
                                     ; address, zeros compress well

blob:
    incbin "TEXT_BLOB"

END:

filesize equ END      - ehdr
phdrsize equ phdr_end - phdr
ahdrsize equ phdr_end - ehdr

