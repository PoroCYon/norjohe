
#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

//#include "elftoc.h"
#include "ELFTOC"

#ifndef ADDR_TEXT
#ifndef ADDR_DATA
#error "No .text section in the binary (not statically linked?)."
#else
// HACK
#define ADDR_TEXT ADDR_DATA
#endif
#endif

static const char* const shevil[] = {
    ".rodata", //".data", ".bss", // TODO: add .bss (and maybe .data) support?
    ".hash", ".dynstr", ".dynsym", ".dynamic", ".rela_dyn"
    ".init", ".init_array", ".fini", ".fini_array",
    ".got", ".got_plt", ".plt", ".rela_plt",
    ".eh_frame", ".eh_frame_hdr",
    NULL
};

int main(int argc, char* argv[]) {
    if (argc < 3) {
        dprintf(STDERR_FILENO, "No output files given.\n");
        return 1;
    }

    if (foo.ehdr.e_machine != EM_386 && foo.ehdr.e_machine != EM_X86_64) {
        dprintf(STDERR_FILENO,
                "Input binary has an unsupported architecture %u.\n",
                foo.ehdr.e_machine);
        return 1;
    }

    // check if sections are OK
    bool seen_text = false;
    for (size_t i = 0; i < foo.ehdr.e_shnum; ++i) {
        const char* sname = &foo.shstrtab[foo.shdrs[i].sh_name];

        if (!strcmp(sname, ".text")) {
            seen_text = true;
        }
        else for (size_t j = 0; shevil[j]; ++j) {
            if (!strncmp(sname, shevil[j], 0x20)) {
                dprintf(STDERR_FILENO, "Found section %s, can't link this!\n",
                        sname);
                if (j == 0) {
                    dprintf(STDERR_FILENO, "Note: it is generally a better idea"
                            " to merge .rodata and .text by using a custom "
                            "linker script.\n");
                }

                return 1;
            }
        }
    }

    if (!seen_text) {
        dprintf(STDERR_FILENO, "No .text section, can't link this!\n");
        return 1;
    }

    const char* textdumppath = argv[1];
    const char* datadumppath = argv[2];

    int filed = creat(textdumppath, 0644);
    if (filed < 0) {
        dprintf(STDERR_FILENO, "Couldn't open output file '%s'\n",
                textdumppath);
        return 1;
    }
    int dafld = creat(datadumppath, 0644);
    if (dafld < 0) {
        dprintf(STDERR_FILENO, "Couldn't open output file '%s'\n",
                datadumppath);
        return 1;
    }

    size_t writesz = sizeof(foo.text);
    uint8_t* textd = foo.text;

    size_t entry = foo.ehdr.e_entry - ADDR_TEXT - offsetof(elf, text);
    size_t extra_zeros = 0;

    size_t textoff = ~(size_t)0;
    size_t data_off = 0, data_mlen = 0, data_flen = 0, data_addr = 0;

    for (size_t i = 0; i < foo.ehdr.e_phnum; ++i) {
        __auto_type ppp = foo.phdrs[i];

        if (ppp.p_type == PT_LOAD) {
            /*dprintf(STDERR_FILENO,
                   "PHDR %zu: flg=%zu, vaddr=%zu, off=%zu, mem=%zu, fls=%zu\n",
                   i, (size_t)ppp.p_flags, ppp.p_vaddr, ppp.p_offset,
                   ppp.p_memsz, ppp.p_filesz);*/
            if ((ppp.p_flags & PF_W) != 0 && !data_off && !data_mlen
                    && !data_addr && !data_flen) {
                data_addr = ppp.p_vaddr;
                data_off  = ppp.p_offset;
                data_mlen = ppp.p_memsz;
                data_flen = ppp.p_filesz;
            }
            else if (((ppp.p_flags & PF_X) != 0) && ppp.p_vaddr == ADDR_TEXT
                    && !~textoff) {

                // ppp is the segment containing .text
                textoff = offsetof(elf, text) - ppp.p_offset;
            } else {
                dprintf(STDERR_FILENO, "Unsupported PHDR %zu: norjohe only "
                        "supports one .text+.rodata and one .data+.bss "
                        "segment.\n", i);
                return 1;
            }
        }
    }

    if (!~textoff) {
        dprintf(STDERR_FILENO,
            "Couldn't find the segment that belongs to the .text section\n");
        return 1;
    }

FOUND_TEXTOFF:
    // shave off some bytes that don't really matter
    if (!entry) {
        for (; writesz > 3; ) {
            switch (*textd) {
                case 0x56: case 0x53: case 0x55: // push rsi/rbx/rbp
                case 0x57: case 0x51:            // push rdi/rcx
                    ++textd; --writesz; ++textoff;
                    break;
                case 0x31: // xor reg, reg
                    textd += 2; writesz -= 2; textoff += 2;
                    break;
                case 0x45: case 0x48:       // xor reg, reg ; inited to 0 by
                    if (textd[1] == 0x31) { // the kernel
                        textd += 3; writesz -= 3; textoff += 3;
                        break;
                    }
                    goto BREAK_OUTER;
                case 0x41:
                    if ((textd[1] & 0xF0) != 0xB0) { // push reg
                        textd += 2; writesz -= 2; textoff += 2;
                        break;
                    }
                  //goto BREAK_OUTER;
                default:
                    goto BREAK_OUTER;
            }
        }
    } else { // if it's not at the start of the file, overwrite it with nops
        uint8_t* ep = &textd[entry]; // for better compression

        for (; ep != &textd[writesz]; ++ep) {
            switch (*ep) {
                case 0x56: case 0x53: case 0x51:
                case 0x57: case 0x55:
                    *ep = 0x90;
                    continue;
                case 0x31:
                    ep[1] = ep[0] = 0x90;
                    ++ep;
                    continue;
                case 0x45: case 0x48:
                    if (ep[1] == 0x31) {
                        ep[2] = ep[1] = ep[0] = 0x90;
                        ep += 2;
                        continue;
                    }
                    goto BREAK_OUTER;
                case 0x41:
                    if ((ep[1] & 0xF0) != 0xB0) {
                        ep[1] = ep[0] = 0x90;
                        ++ep;
                        continue;
                    }
                  //goto BREAK_OUTER;
                default:
                    goto BREAK_OUTER;
            }
        }
    }
BREAK_OUTER:;

    // trim trailing zeros, as it will be zeroed out by the kernel when it
    // creates the page mapping anyway
    for (; writesz > 1 && !textd[writesz - 1]; --writesz, ++extra_zeros) ;
    ssize_t wrres = write(filed, textd, writesz);
    close(filed);
    if (wrres < 0 || (size_t)wrres != writesz) {
        dprintf(STDERR_FILENO, "Couldn't write to output file '%s'\n",
                textdumppath);
        return 1;
    }

    // give the relevant data to the rest of the linker:
    printf(
"export EI_CLASS=%u\n"
"export EI_OSABI=%u\n"
"export E_ENTRY=%zu\n"
"export ADDR_TEXT=%zu\n"
"export EXTRA_ZEROS=%zu\n"
"export TEXT_OFF=%zu\n",
        foo.ehdr.e_ident[EI_CLASS],
        foo.ehdr.e_ident[EI_OSABI],
        entry,
        (size_t)ADDR_TEXT,
        extra_zeros,
        textoff);

    if (data_off) {
        uint8_t* datad = (uint8_t*)&foo + data_off;

        //for (; data_flen > 1 && !datad[data_flen - 1]; --data_flen) ;
        ssize_t dawrres = write(dafld, datad, data_flen);
        close(dafld);
        if (dawrres < 0 || (size_t)dawrres < data_flen) {
            dprintf(STDERR_FILENO, "Couldn't write to output file '%s'\n",
                    datadumppath);
            return -1;
        }

        printf(
"export HAS_DATA=1\n"
"export ADDR_DATA=%zu\n"
"export DATA_OFF=%zu\n"
"export DATA_MEMSZ=%zu\n"
"export DATA_FILESZ=%zu\n",
              data_addr,
              data_off,
              data_mlen,
              data_flen);
    } else {
        close(dafld);
      //printf("export HAS_DATA=0\n");
    }

    return 0;
}

