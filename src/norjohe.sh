#!/usr/bin/env bash

#set -x

# 0x00: initial not-really-release
# 0x01: added -i, -p
# 0x02: added .data, .bss support (.rodata: just put it in .text with a ldscpt)
NRJH_VER="0x02"

print_version() {
    echo "norjohe v. ${NRJH_VER}: not really a linker." >&2
}
print_usage() {
    print_version
    cat >&2 <<EOF
Links a static ELF executable into a smaller one.

NOTE: the following things must *NOT* appear in the input binary:
	* ~~Position-dependent code~~ Should work? Kinda, at least.
	* Dynamically linked symbols
	* ~~.data, .rodata, .bss and~~ .hash sections
	  * .data and .bss are supported as of v0x02. .rodata isn't supported
	    out of the box, just merge it with .text in your linker script.
	* Initializers and finalizers (unless handled manually)
	* A GOT or PLT
	* Exception handler frame data

These conditions are usually fulfilled when one compiles & links with
\`-nostdlib -nostartfiles -static -fno-plt (-fno-pie -no-pie)'.

Currently, only x86 and x86_64 binaries are supported.

Usage:
$(basename "$0")	[-h|--help] [-V|--version] [-i|--pic] [-p|--python]
		<input file> <output file>

	-h	Display this help text and exit.
	-V	Print the version number and exit.
	-i	Use another hack to make the resulting file size even smaller,
		but this only works when the program contains no absolute memory
		accesses.
	-p	Use a Python loader instead of an ELF header. Has the same
		limitations as the \`-i' option. Usage of this flag requires
		Python to be installed.

		The \`-i' and \`-p' flags cannot be combined. If one of these flags
		is used an the binary does contain absolute memory accesses, norjohe
		will succeed but the output will most likely segfault.

$(basename "$0") needs the program \`elftoc', a C compiler
and the YASM assembler in order to operate. The programs used for
this can be controlled using the NRJH_{ELFTOC,CC,ASM} environment variables.
The directory of the \`resource' files (i.e. the C and assembly templates)
can be selected using the NRJH_RES variable, which defaults to
\`INSTALL_PREFIX/libexec/norjohe'.
EOF
}

INPUT="$1"
OUTPUT="$2"
USE_PIC_HACK=0
USE_PYL_HACK=0

if [[ -z "$NRJH_ELFTOC" ]]; then
    NRJH_ELFTOC="elftoc"
fi
if [[ -z "$NRJH_CC" ]]; then
    NRJH_CC="$CC"
fi
if [[ -z "$NRJH_CC" ]]; then
    NRJH_CC="cc"
fi
if [[ -z "$NRJH_ASM" ]]; then
    NRJH_ASM="$YASM"
fi
if [[ -z "$NRJH_ASM" ]]; then
    NRJH_ASM="$AS"
fi
if [[ -z "$NRJH_ASM" ]]; then
    NRJH_ASM="yasm"
fi
if [[ -z "$NRJH_RES" ]]; then
    NRJH_RES="INSTALL_PREFIX/libexec/norjohe"
fi

die() {
    if [[ -d "$TEMPDIR" ]]; then
        #true
        rm -rf "$TEMPDIR"
    fi
    exit $1
}

escape_for_sed() {
    echo "$1" | sed -E 's/[\/&]/\\&/g'
}

parse_args() {
    case "$1" in
        "-h"|"--help")
            print_usage
            exit
            ;;
        "-V"|"--version")
            print_version
            exit
            ;;
        "-i"|"--pic")
            if [[ $USE_PYL_HACK -ne 0 ]]; then
                echo "Cannot combine \`-i' and \`-p' flags." >&2
                exit
            fi

            USE_PIC_HACK=1

            local ALLARGS=("$@")
            parse_args "${ALLARGS[@]:1}"
            return $?
            ;;
        "-p"|"--python")
            if [[ $USE_PIC_HACK -ne 0 ]]; then
                echo "Cannot combine \`-i' and \`-p' flags." >&2
                exit
            fi

            USE_PYL_HACK=1

            local ALLARGS=("$@")
            parse_args "${ALLARGS[@]:1}"
            return $?
            ;;
        *)
            if [ $# -ne 2 ]; then
                print_usage
                exit
            fi

            INPUT="$1"
            OUTPUT="$2"
            ;;
    esac
}

if [ $# -eq 0 ]; then
    print_usage
    exit
fi

parse_args "$@"

if ! command -v "$NRJH_ELFTOC" >/dev/null 2>&1; then
    echo "\`${NRJH_ELFTOC}' command not found." >&2
    exit 1
fi
if ! command -v "$NRJH_CC" >/dev/null 2>&1; then
    echo "\`${NRJH_CC}' command not found." >&2
    exit 1
fi
if ! command -v "$NRJH_ASM" >/dev/null 2>&1; then
    echo "\`${NRJH_ASM}' command not found." >&2
    exit 1
fi

if ! [[ -f "$INPUT" ]]; then
    echo "Input file '${INPUT}' not found." >&2
    exit 1
fi

TEMPDIR="/tmp/$(head -c 5 /dev/urandom | base32)"
mkdir -p "$TEMPDIR"

ELFTOC_OUT="$TEMPDIR/elftoc.h"

LINKER_SRC="$NRJH_RES/linker.pre.c"
LINKER_MOD="$TEMPDIR/linker.c"
LINKER_BIN="$TEMPDIR/linker"
LINKER_VAR="$TEMPDIR/linker-vars.sh"
LINKER_OUT="$TEMPDIR/text.bin"
LNDATA_OUT="$TEMPDIR/data.bin"

if ! "$NRJH_ELFTOC" "$INPUT" > "$ELFTOC_OUT"; then
    echo "elftoc error." >&2
    die 1
fi

< "$LINKER_SRC" sed -E "s/ELFTOC/$(escape_for_sed "$ELFTOC_OUT")/" > "$LINKER_MOD"
if ! "$NRJH_CC" -o "$LINKER_BIN" "$LINKER_MOD"; then
    echo "C compiler error." >&2
    die 1
fi

if ! "$LINKER_BIN" "$LINKER_OUT" "$LNDATA_OUT" > "$LINKER_VAR"; then
    echo "Extractor error." >&2
    die 1
fi

. "$LINKER_VAR"

if [[ $USE_PYL_HACK -ne 0 ]]; then
    if ! [[ -z "$HAS_DATA" ]]; then
        >&2 echo "Can't combine a .data/.bss section with -p."
        die 1
    fi

    PYSCR="$TEMPDIR/scr.py"

    PYHDR_SRC="$NRJH_RES/loader-64.pre.py"
    if [[ $EI_CLASS -eq 1 ]]; then
        PYHDR_SRC="$NRJH_RES/loader-32.pre.py"
    fi

    # if they're using -p, they probably have python installed anyway
    cat >"$PYSCR" <<EOF
import sys

PYHDR_SRC = sys.argv[1]
TEXT_BLOB = sys.argv[2]
OUTPUT    = sys.argv[3]

DATA_LENGTH = "DATA_LENGTH"
DATA_OFF    = "DATA_OFF"

def readfile(path, binary=False, encoded=False):
    filed = open(path, 'rb' if binary else 'r')
    retv = filed.read()
    filed.close()
    return retv if not encoded else retv.encode('utf-8')

# TODO: make these suck less
def strlenb(s): return len(s.encode('utf-8'))
def numlenb(s): return strlenb(str(s))

pyhdr = readfile(PYHDR_SRC)
textd = readfile(TEXT_BLOB, binary=True)

# calculate DATA_LENGTH and DATA_OFF
baseo = strlenb(pyhdr) - (strlenb(DATA_LENGTH) + strlenb(DATA_OFF))

do = baseo
dl = do + len(textd)

while True:
    #print("do="+str(do)+", dl="+str(dl))
    oo, ol = do, dl

    do = baseo + numlenb(do) + numlenb(dl)
    dl = do + len(textd)

    if ol == dl and oo == do:
        #print("do="+str(do)+", dl="+str(dl))
        break

# now write
outf = open(OUTPUT, 'wb')
outf.write(pyhdr.replace(DATA_LENGTH, str(dl)).replace(DATA_OFF, str(do)).encode('utf-8'))
outf.write(textd)
outf.close()
EOF
    python3 "$PYSCR" "$PYHDR_SRC" "$LINKER_OUT" "$OUTPUT" && chmod +x "$OUTPUT"
else
    ASMHDR_SRC="$NRJH_RES/elfhdr-64.pre.asm"
    if [[ $EI_CLASS -eq 1 ]]; then
        ASMHDR_SRC="$NRJH_RES/elfhdr-32.pre.asm"
    fi
    ASMHDR_MOD="$TEMPDIR/elfhdr.asm"

    < "$ASMHDR_SRC" sed -E -e "s/TEXT_BLOB/$(escape_for_sed "$LINKER_OUT")/" \
                           -e "s/DATA_BLOB/$(escape_for_sed "$LNDATA_OUT")/" \
        > "$ASMHDR_MOD"

    PIC_ARG=""
    if [[ $USE_PIC_HACK -ne 0 ]]; then
        PIC_ARG="-DIS_PIC=1"
    fi
    if ! [[ -z "$HAS_DATA" ]]; then
        if [[ $USE_PIC_HACK -ne 0 ]]; then
            >&2 echo "WARN: input file has .bss/.data, this disables -i."
        fi
        PIC_ARG="-DHAS_DATA=1"
    fi

    if ! "$NRJH_ASM" -DPAGE_SIZE="$(getconf PAGESIZE)" \
            -DEI_OSABI="$EI_OSABI" -DSTART_OFF="$E_ENTRY" \
            -DADDR_TEXT="$ADDR_TEXT" -DEXTRA_ZEROS="$EXTRA_ZEROS" \
            -DTEXT_OFF="$TEXT_OFF" $PIC_ARG \
            -DADDR_DATA="$ADDR_DATA" -DDATA_MEMSZ="$DATA_MEMSZ" \
            -DDATA_FILESZ="$DATA_FILESZ" -DDATA_OFF="$DATA_OFF" \
            -f bin -o "$OUTPUT" "$ASMHDR_MOD"; then
        echo "Assembler error." >&2
        die 1
    fi

    chmod +x "$OUTPUT"
fi

die 0

