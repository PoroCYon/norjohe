; vim: set ft=nasm noet:

; to define:
; * START_OFF
; * ADDR_TEXT
; * EI_OSABI    ; not really needed
; * TEXT_OFF
; * IS_PIC

%ifndef EXTRA_ZEROS
%define EXTRA_ZEROS 0
%endif

%ifdef IS_PIC
%define ADDR_TEXT 0x00200000
;%define START_OFF 0
%define TEXT_OFF ahdrsize
%endif

%define PT_LOAD 1
%define PF_X    1
%define PF_W    2
%define PF_R    4
%define PF_RX   5
%define PF_RW   6

bits 32
org ADDR_TEXT

ehdr:
	db 0x7F, "ELF"  ;!e_ident       ;!EI_MAG[0-3]
	db  0           ; EI_CLASS      ; 1: 32-bit, 2: 64-bit
	db 'K'          ; EI_DATA       ; 1 = 2's compl., LE ; 2 = 2's compl., BE
	db '2'          ; EI_VERSION    ; 1 = current
	db  0           ; EI_OSABI      ; 0 = SYSV, 3 = GNU/Linux, 2 = NetBSD, 9 = FreeBSD, 12 = OpenBSD
	db "pcy!nrjh"   ; padding
	dw  2           ;!e_type        ; 2 = executable, 3 = shared obj, 4 = coredump
	dw  3           ;!e_machine     ; 3 = x86, 62 = x86_64
	dd  0           ; e_version     ; 1 = current
	dd blob + START_OFF ;!e_entry
	dd phdr - ehdr  ;!e_phoff
%ifndef IS_PIC
	dd 0            ; e_shoff
	dd 0            ; e_flags
	dw 0            ; e_ehsize
	dw 32;phdrsize     ;!e_phentsize
%ifndef HAS_DATA
phdr: ; TODO: recheck for more merging
	dd PT_LOAD      ;!p_type        ;!e_phnum, e_shentsize
	dd 0            ;!p_offset      ; e_shnum, e_shstrndx
	dd ADDR_TEXT    ;!p_vaddr
	dd 0            ; p_paddr
	dd textsize     ;~p_filesz      ; -> can be larger than needed, but have to be equal
	dd textsize     ;~p_memsz       ; -> ^
	dd PF_RX        ;~p_flags       ; needs to have at least one of 1|4 (one implies the other)
	dd 0            ; p_align
%else
	dw 2            ;               ;!e_phnum
phdr:
phdr.text:
	; LOAD 0 R-E: .text, .rodata
	dd PT_LOAD      ;!p_type        ; e_shentsize, e_shnum
	dd 0            ;!p_offset      ; e_shstrndx
	dd ADDR_TEXT    ;!p_vaddr
	dd ADDR_TEXT    ; p_paddr
	dd textsize     ;~p_filesz
	dd textsize     ;~p_memsz
	dd PF_RX        ;~p_flags
	dd 0x1000       ; p_align
phdr.data:
	; LOAD 1 RW-: .data, .bss
	dd PT_LOAD      ;!p_type
	dd blob.data-ehdr;!p_offset
	dd ADDR_DATA    ;!p_vaddr
	dd ADDR_DATA    ; p_paddr
	dd DATA_FILESZ  ;~p_filesz
	dd DATA_MEMSZ   ;~p_memsz
	dd PF_RW        ;~p_flags
	dd 0x1000       ; p_align
%endif
%else
phdr:
	dd PT_LOAD      ; e_shoff       ;!p_type
	dd 0            ; e_flags       ;!p_offset
	dd ADDR_TEXT    ; e_ehsize      ;!p_vaddr
	                ;!e_phentsize
	dw 1            ;!e_phnum       ; p_paddr
	dw 0            ; e_shentsize
	dd textsize     ; e_shnum       ;~p_filesz  ; see above
	                ; e_shstrndx
	dd textsize                     ;~p_memsz   ; see above
	dd PF_RX                        ;~p_flags   ; see above
	dd 0                            ; p_align
%endif
phdr.end:
	times (TEXT_OFF - ahdrsize) db 0 ; ensure the blob starts at the old
                                     ; address, zeros compress well

blob:
	incbin "TEXT_BLOB"
blob.text.end:

%ifdef HAS_DATA
	times (DATA_OFF - (blob.text.end - ehdr)) db 0
blob.data:
	incbin "DATA_BLOB"
%endif

END:

%ifdef HAS_DATA
textsize equ blob.data- ehdr
%else
textsize equ END      - ehdr
%endif
filesize equ END      - ehdr
phdrsize equ phdr.end - phdr
ahdrsize equ phdr.end - ehdr

